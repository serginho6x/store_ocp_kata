package it.xpug.ocp.customerbase;

import static java.util.Arrays.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Before;

import java.util.*;

import org.junit.*;


public class CustomerBaseTest {

	private Customer alice = new Customer("Alice", "Rossi", 10000);
	private Customer bob = new Customer("Bob", "Rossi", 20000);
	private Customer charlie = new Customer("Charlie", "Bianchi", 30000);

	private CustomerBase customerBase = new CustomerBase();

	@Before
	public void setUp() throws Exception {
		customerBase.add(alice);
		customerBase.add(bob);
		customerBase.add(charlie);
	}
	
	@Test
	public void findByLastName() throws Exception {
		Customer model = new Customer("Alice", "Rossi", 10000);
		List<Customer> found = customerBase.FinderCustomer(model, 1);
		assertThat(found, is(asList(alice, bob)));
	}
	@Test
	public void findByFirstAndLastName() throws Exception {
		Customer model = new Customer("Alice", "Rossi", 10000);
		List<Customer> found = customerBase.FinderCustomer(model, 2);
		assertThat(found, is(asList(alice)));
	}

	@Test
	public void findWithCreditGreaterThan() throws Exception {
		Customer model = new Customer("Alice", "Rossi", 20000);
		List<Customer> found = customerBase.FinderCustomer(model,3);
		assertThat(found, is(asList(charlie)));
	}



}
