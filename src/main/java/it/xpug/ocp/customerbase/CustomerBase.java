package it.xpug.ocp.customerbase;
import java.util.*;


public class CustomerBase {

	private List<Customer> customers = new ArrayList<Customer>();

	public void add(Customer customer) {
		customers.add(customer);
	}
	
	public List<Customer> FinderCustomer(Customer customer, int Option) {
		
		List<Customer> result = null;
		Finder_Interface Finder = SelectFinder(Option);
		result = Finder.FindCustomer(customer, customers);
		return result;
	}

	private Finder_Interface SelectFinder(int Option)
	{
		Finder_Interface result = null;
		if (Option == 1)
			result = new FinderByLastName();
		if (Option == 2)
			result = new FinderByLastAndFirstName();
		if (Option == 3)
			result = new FinderByCreditGreaterThan();
		return result;
	}


}
