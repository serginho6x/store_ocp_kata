package it.xpug.ocp.customerbase;

import java.util.List;

public interface Finder_Interface {
	public List<Customer> FindCustomer(Customer customer, List<Customer> customers);
}
