package it.xpug.ocp.customerbase;

import java.util.ArrayList;
import java.util.List;

public class FinderByCreditGreaterThan implements Finder_Interface{
	
	@Override
	public List<Customer> FindCustomer(Customer customer, List<Customer> customers) {
		List<Customer> result = new ArrayList<Customer>();
		for (Customer IteratorCustomer : customers) {
			if (IteratorCustomer.credit() > customer.credit()) {
				result.add(IteratorCustomer);
			}
		}
		return result;
	}

}
